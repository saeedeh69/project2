<?php
namespace MyApp\Models;

use Phalcon\Mvc\Model;

class Products extends Model
{
    public $id;
    public $categories;
    public $name;
    public $price;

    public function initialize()
    {
        $this->setSource('product');   
    }
}