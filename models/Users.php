<?php
namespace MyApp\Models;

use Phalcon\Mvc\Model;

class Users extends Model
{
    public $id;
    public $age;
    public $name;
    public function initialize()
    {
        $this->setSource('user');
    }
}