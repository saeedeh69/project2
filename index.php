<?php
use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use MyApp\Models\Users;
use MyApp\Models\Products;
use Phalcon\Http\Response;
use Phalcon\Http\Request;

$loader = new Loader();
$loader->registerNamespaces(
    [
        'MyApp\Models' => __DIR__ . '/models/',
    ]
);
$loader->register();
$di = new FactoryDefault();
$app = new Micro();

$app->after(
    function () use ($app) {
        $app
            ->response
            ->setJsonContent($app->getReturnedValue())
            ->send();
    }
);

$di['url'] = function () {
    $url = new UrlResolver();
    $url->setBaseUri("/pr7");
    return $url;
};

$di->set(
    'db',
    function () {
        return new PdoMysql(
            [
                'host'     => 'localhost',
                'username' => 'root',
                'password' => '',
                'dbname'   => 'test',
            ]
        );
    }
);

$app->setDI($di);

$app->get(
    '/users',
    function () use ($app) {
        $users = Users::find();
        return $users->toArray();
    }
);

$app->get(
    '/users/{id:[0-9]+}',
    function ($id) use ($app) {  
        $user = Users::findFirstById($id); 
        if ($user !== false){
            $app->response->setStatusCode(206, 'Partial Content');
            return $user->toArray();
        } else {
            $app->response->setStatusCode(404, 'Not Found');
            return ["message" => "sorry, not found user"];         
        }
    }
);

$app->DELETE(
    '/users/{id:[0-9]+}',
    function ($id) use ($app) {  
        $user = Users::findFirstByid($id);
        if($user !== false) {
            if ($user->delete() === true)
            return $user->toArray();
        } else {
            $app->response->setStatusCode(404, 'Not Found');
            return ["message" => "sorry, not found user"];
        }
    }
);

$app->post(
    '/users',
    function() use ($app) {
        $user = new Users();
        $request = new Request();
        $user->age = $request->getpost('age');
        $user->name = $request->getpost('name');
        if ($user->save() === false) {
            $message = $user->getMessages();
            foreach($message as $message) {
                $showMessage[] = $message->getMessage();
            }
            $app->response->setStatusCode(406, 'Not Acceptable');
            return [$showMessage];       
        } else {
            $app->response->setStatusCode(201, 'Created');
            return  $user->toArray();
        }
    }
);

$app->put(
    '/users/{id:[0-9]+}',
    function($id) use ($app) {
        $request = new Request();
        $user = Users::findFirst($id);
        $body = $request->getJsonRawBody(true);
        if ($user !== false) {
            $user->age = $body['age'];
            $user->name = $body['name'];
            if ($user->update() === true) {
                return  $user->toArray();
            }
        } else {
            $app->response->setStatusCode(406, 'Not Acceptable');
            return ["message" => "sorry, don't update user"]);        
        }
    }
);

$app->get(
    '/products',
    function () use ($app) {
        $products = Products::find();
        return $products->toArray();
    }
);

$app->get(
    '/products/{id:[0-9]+}',
    function ($id) use ($app) {  
        $product = Products::findFirstById($id); 
        if ($product !== false) {
           $app->response->setStatusCode(206, 'Partial Content');
           return $product->toArray();         
        } else {
           $app->response->setStatusCode(404, 'Not Found');
           return ["message" => "sorry, not found user"];          
        }
    }
);

$app->DELETE(
    '/products/{id:[0-9]+}',
    function ($id) use ($app) {  
        $products = Products::findFirstByid($id);
        if($products !== false) {
           if($products->delete() === true)
              return $products->toArray();
        } else {
            $app->response->setStatusCode(404, 'Not Found');
            return ["message" => "sorry, not found product"];              
        }
    }
);

$app->post(
    '/products',
    function() use ($app) {
        $products = new Products();
        $request = new Request();
        $products->name = $request->getpost('name');
        $products->categories = $request->getpost('categories');
        $products->price = $request->getpost('price');
        if ($products->save() === false) {
            $message = $products->getMessages();
            foreach($message as $message) {
                $showMessage[] = $message->getMessage();
            }
            $app->response->setStatusCode(406, 'Not Acceptable');   
            return [$showMessage];  
        } else {
            $app->response->setStatusCode(201, 'Created');
            return $products->toArray();
        }
    }
);

$app->put(
    '/products/{product_id:[0-9]+}',
    function($product_id) use ($app) {
        $request = new Request();
        $products = Products::findFirst($product_id);
        $body = $request->getJsonRawBody(true);
        if ($products !== false) {
            $products->categories = $body['categories'];
            $products->name = $body['name'];
            $products->price = $body['price'];
            if ($products->update() === true) {
                return  $products->toArray();
            }
        } else {
            $app->response->setStatusCode(406, 'Not Acceptable');
            return ["message" => "sorry, don't update product"];
        }
    }
);
$app->handle();